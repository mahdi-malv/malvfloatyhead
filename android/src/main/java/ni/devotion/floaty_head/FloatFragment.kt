package ni.devotion.floaty_head

import android.content.Context
import android.content.res.Configuration
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.facebook.rebound.SimpleSpringListener
import com.facebook.rebound.Spring
import com.facebook.rebound.SpringSystem
import ni.devotion.floaty_head.floating_chathead.SpringConfigs
import ni.devotion.floaty_head.R
import ni.devotion.floaty_head.utils.Managment.bodyView
import ni.devotion.floaty_head.utils.Managment.footerView
import ni.devotion.floaty_head.utils.Managment.headerView
import android.view.ViewGroup





class FloatFragment(context: Context) : LinearLayout(context) {

    companion object {
        @JvmStatic
        var instance: FloatFragment? = null
    }

    private val springSystem = SpringSystem.create()
    private val scaleSpring = springSystem.createSpring()
    private lateinit var content: LinearLayout

    init {
        setupView()
        instance = this
    }

    // VisibleForFastDeployment
    fun setupView() {
        context.setTheme(R.style.Theme_MaterialComponents_Light)
        inflate(context, R.layout.fragment_float, this)
        scaleSpring.addListener(object : SimpleSpringListener() {
            override fun onSpringUpdate(spring: Spring) {
                scaleX = spring.currentValue.toFloat()
                scaleY = spring.currentValue.toFloat()
            }
        })
        scaleSpring.springConfig = SpringConfigs.CONTENT_SCALE
        scaleSpring.currentValue = 0.0
        content = findViewById(R.id.contentLayout)
        content.removeAllViews()
        val w = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val width = w.defaultDisplay.width
        val height = w.defaultDisplay.height
        val isLand = width >= height

        val p = FrameLayout.LayoutParams(
          if (isLand) height * 4/5 else width * 9/10,
          if (isLand) height * 9/10 else width * 10/9
        )

        if(isLand) {
            p.gravity = Gravity.RIGHT or Gravity.TOP
            p.rightMargin = 120
            p.topMargin = 20
        } else {
            p.gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
            p.topMargin = 120
        }

        content.layoutParams = p
        try {
            headerView?.let {
                if (it.getParent() != null) {
                    (it.getParent() as ViewGroup).removeView(it) // <- fix
                }
                content.addView(it)
            }
            bodyView?.let {
                if (it.getParent() != null) {
                    (it.getParent() as ViewGroup).removeView(it) // <- fix
                }
                content.addView(it)
            }
            footerView?.let {
                if (it.getParent() != null) {
                    (it.getParent() as ViewGroup).removeView(it) // <- fix
                }
                content.addView(it)
            }
        } catch (e: java.lang.Exception) {
            println("Error adding views to FloatFragment: ${e.message}")
        }
    }

    override fun onViewRemoved(child: View?) {
        super.onViewRemoved(child)
        content.removeAllViews()
    }

    fun hideContent() {
        scaleSpring.endValue = 0.0
        val anim = AlphaAnimation(1.0f, 0.0f)
        anim.duration = 40
        anim.repeatMode = Animation.RELATIVE_TO_SELF
        startAnimation(anim)
    }

    fun showContent() {
        scaleSpring.endValue = 1.0
        val anim = AlphaAnimation(0.0f, 1.0f)
        anim.duration = 40
        anim.repeatMode = Animation.RELATIVE_TO_SELF
        startAnimation(anim)
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        if(newConfig == null) return
        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setupView()
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            setupView()
        }
    }
}