package ni.devotion.floaty_head.models

import android.content.Context
import ni.devotion.floaty_head.utils.Commons
import ni.devotion.floaty_head.utils.NumberUtils

/**
 * assetPath is the background image
 * foregroundAssetPath is the image set on top of assetPath
 * tag is a unique string attached to this decoration
 */
class Decoration(startColor: Any?,
                 endColor: Any?,
                 borderWidth: Any?,
                 borderRadius: Any?,
                 borderColor: Any?,
                 assetPath: Any?,
                 foregroundAsset: Any?,
                 tag: Any?,
                 disabled: Any?,
                 context: Context?) {
    val startColor: Int
    var endColor = 0
    val borderWidth: Int
    val borderRadius: Float
    val borderColor: Int
    val assetPath: String?
    val foregroundAsset: String?
    val tag: String
    var isGradient = false
    var disabled = false

    init {
        this.startColor = NumberUtils.getInt(startColor)
        if (endColor != null) {
            this.endColor = NumberUtils.getInt(endColor)
            isGradient = true
        } else {
            isGradient = false
        }
        this.assetPath = (assetPath as? String) ?: ""
        this.foregroundAsset = (foregroundAsset as? String) ?: ""
        this.tag = (tag as? String) ?: ""
        this.borderWidth = Commons.getPixelsFromDp(context!!, NumberUtils.getInt(borderWidth))
        this.borderRadius = Commons.getPixelsFromDp(context, NumberUtils.getFloat(borderRadius))
        this.borderColor = NumberUtils.getInt(borderColor)
        this.disabled = (disabled as? Boolean) ?: false
    }
}