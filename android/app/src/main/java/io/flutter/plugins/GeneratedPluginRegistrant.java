package io.flutter.plugins;

import io.flutter.plugin.common.PluginRegistry;
import ni.devotion.floaty_head.FloatyHeadPlugin;

/**
 * Generated file. Do not edit.
 */
public final class GeneratedPluginRegistrant {
  public static void registerWith(PluginRegistry registry) {
    if (alreadyRegisteredWith(registry)) {
      return;
    }
    FloatyHeadPlugin.registerWith(registry.registrarFor("ni.devotion.floaty_head.FloatyHeadPlugin"));
  }

  private static boolean alreadyRegisteredWith(PluginRegistry registry) {
    final String key = GeneratedPluginRegistrant.class.getCanonicalName();
    if (registry.hasPlugin(key)) {
      return true;
    }
    registry.registrarFor(key);
    return false;
  }
}
